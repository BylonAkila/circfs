/*
  circfs: Circular File System

  Copyright (C) 2022-2022  Alain BENEDETTI <alainb06@free.fr>

  License:
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  Feature:
	Apparently, GNU/Linux does not have an all purpose circular file tool.
	The goal is to limit the size of evergrowing log to a maximum amount,
	loosing in the process oldest entries if the amount is exceeded.
	There is dmesg for kernel log, and some tools like emblog which is
	very limited in size (targetted at "embedded" systems) and tools in
	daemontools that rotate logs but also limited to 16Mb per log file.

	This tool is very basic to remove all those limits.
	It works a bit like encfs, the user provides:
	- a real directory were raw logs will be written to
	- a mount directory that is where "circular" logs go
	- a maximum size

	When writing a file to the mountpoint, the same file will be written
	to the real directory. The "real" file will be kept at the maximum size
	and start to "rotate" from there.
	The file on the mount would on the contrary seem to grow indefinitely
	When the file is closed, the real file is re-written so that it can
	be displayed with standard tools (cat and all).
	So in fact the space needed on the storage is twice the maximum for
	each file written.

	The user can write as many files as needed on the mountpoint, they
	all have the same maximum size.

	Since this is (for now) very basic, the fuse driver does NOT provide
	reading of file. The files (although "circular") can be accessed
	in the real directory instead.

	So in fact, at the moment this is a "Write-only" fuse driver. ;-)

  Usage:
	circfs storage_dir mountpoint max_size_per_file [fuse options]

	The size expression can use standard multipliers: k K, m M, g G, t T
	k = 1000
	K = 1024
	etc...


  Typical use case:
	Limit log files. I am using it on RaspPi with 2GB of /tmp RAM it is
	too small to track errors on my other fuse driver: 1fichierfs when in
	debug mode. Limiting to 500M is much enough to see the last events
	before a potential crash

  Compile with:
	You only need one file to compile: circfs.c

	cc -O2 -Wall -std=c99 -Wpedantic circfs.c `pkg-config fuse --cflags --libs` -o circfs

  Compile dependancy:
	You must have libfuse-dev installed, and standard compile tools (generally
	already installed as default in your distribution)
	Typically install libfuse-dev with:
	(Debian/Ubuntu) sudo apt-get install libfuse-dev
	(Fedora)        sudo dnf install fuse-devel
	... or adapt with the package manager that comes with your distribution.


  Version: 0.9.1

  History:
	2022/02/08: 0.9.1 Buffering writes: ~20% faster (+fix unlock missing)
	2022/01/30: 0.9.0 Initial basic version (no read)


  TODO:
	Possibly provide read to avoid letting the user trying to interpret
	the circular files.
	Option to "not" buffer (useless if read is developped, then code can
	be optimised)
	Possibly write in a thread to avoid "garbage collection effect" of
	operations slowing down when a write is needed.
*/

#define _GNU_SOURCE
#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdarg.h>
#include <syslog.h>
#include <string.h>
#include <pthread.h>
#include <linux/fs.h>
#include <sys/ioctl.h>

#define ST_BLK_SZ (512)		/* This is the block size stat's st_blocks */
static struct stat mount_st;	/* Stat of the mount path as base to respond to
				   getattr and readir */
static uintmax_t max_size;	/* Maximum size requested */
static char  *storage_dir;
static size_t storage_dir_strlen;

static const char units[]="bBkKmMgGtT";
static const unsigned long multiple[]=
{
	1UL				, 1UL,
	1000UL				, 1024UL,
	1000UL*1000UL			, 1024UL*1024UL,
	1000UL*1000UL*1000UL 		, 1024UL*1024UL*1024UL,
	1000UL*1000UL*1000UL*1000UL	, 1024UL*1024UL*1024UL*1024UL
};

#define BUF_SZ 65536 /* Buf sz to avoid calling pwrite at each fuse request */

struct circ {
	struct circ    *next;
	int		store_fh;
	int		n_open;
	mode_t		mode;
	uintmax_t	size;
	off_t		buf_off;
	size_t		buf_used;
	char		buf[BUF_SZ];
	char		path[];
};

static struct circ *head = NULL; /* head of all currently opened files */

static pthread_mutex_t	main_lock = PTHREAD_MUTEX_INITIALIZER;


/*
 * @brief find an open file in the linked list
 *
 * @param path to search for
 * @return pointer on the struc circ if found, NULL if not found
 */
static struct circ *find(const char *path)
{
	struct circ *cc;
	for (cc = head; NULL != cc; cc = cc->next)
		if (0 == strcmp(path, cc->path))
			return cc;
	return NULL;
}

/*
 * @brief fuse callback for getattr
 *
 * @param path to the file whose attributes are queried
 * @param stat buffer to return the attributes
 * @return 0 if ok or error code
 */
static int circfs_getattr(const char *path, struct stat *stbuf)
{
	int res = 0;
	struct circ *cc;

	memset(stbuf, 0, sizeof(struct stat));
	if (strcmp(path, "/") == 0) {
		memcpy(stbuf, &mount_st, sizeof(struct stat));
	}
	else {
		if (0 != pthread_mutex_lock(&main_lock))
			return -ENOENT;
		cc = find(path);
		if (NULL == cc) {
			res = -ENOENT;
		}
		else {
			memcpy(stbuf, &mount_st, sizeof(struct stat));
			stbuf->st_mode = cc->mode | S_IFREG;
			stbuf->st_nlink= cc->n_open;
			stbuf->st_size = cc->size;
			stbuf->st_blocks = stbuf->st_size / ST_BLK_SZ;
		}
		pthread_mutex_unlock(&main_lock);
	}
	return res;
}

/*
 * @brief fuse callback for readdir
 *
 * @param path of the directory to read
 * @param buffer for the filler function
 * @param function to fill entries
 * @param offset for incremental readdir mode (not used)
 * @param fuse file infor pointer for incremental readdir mode (not used)
 * @return 0 if ok or error code
 */
static int circfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
			off_t offset_unused, struct fuse_file_info *fi_unused)
{
	(void) offset_unused;
	(void) fi_unused;
	struct circ *cc;
	int res;

	if (strcmp(path, "/") != 0)
		return -ENOENT;

	if ( 0 == filler(buf, ".", &mount_st,  0) &&
	     0 == filler(buf, "..", &mount_st, 0)
	   ) {
		res = 0;
		if (0 != pthread_mutex_lock(&main_lock))
			return 0;
		for (cc = head; cc != NULL; cc = cc->next)
			if (0 != filler(buf, cc->path + 1, NULL, 0)) {
				res = -EBADF;
				break;
			}
		pthread_mutex_unlock(&main_lock);
	}
	else {
		res = -EBADF;
	}
	return res;
}

/*
 * @brief fuse callback for create
 *
 * Same as opening a file, but for writing.
 *
 * @param path to the file being created + opened
 * @param mode for the file to be created (not support by the remote)
 * @param fuse_file_info pointer
 * @return 0 if ok or error code
 */
static int circfs_create(const char *path, mode_t mode,
			 struct fuse_file_info *fi)
{
	int fh, res = 0;
	size_t path_sz;
	struct circ *cc;

	/* access mode cannot be 'read only' for now */
	if ((fi->flags & O_ACCMODE) == O_RDONLY)
		return -EACCES;
	/* access mode here is one of 'write only' or 'read write' */
	if (0 != pthread_mutex_lock(&main_lock))
		return -errno;
	cc = find(path);
	if (NULL != cc) {
		/* The file was already opened */
		cc->n_open++;
		fi->fh = (uintptr_t)cc;
	}
	else {
		/* New file: open it in storage_dir and add it to the list */
		path_sz = strlen(path) + 1;
		char full_path[storage_dir_strlen + path_sz];

		memcpy(full_path, storage_dir, storage_dir_strlen);
		strcpy(full_path + storage_dir_strlen, path);
		fh = open(full_path, O_RDWR | O_CREAT | O_EXCL, mode);
		if (-1 == fh) {
			res = -errno;
		}
		else {
			cc = malloc(sizeof(struct circ) + path_sz);
			if (NULL == cc) {
				res = -ENOMEM;
			}
			else {
				cc->next     = head;
				cc->store_fh = fh;
				cc->n_open   = 1;
				cc->mode     = mode;
				cc->size     = 0;
				cc->buf_off  = 0;
				cc->buf_used = 0;
				strcpy(cc->path, path);
				head = cc;
				fi->fh = (uintptr_t)cc;
			}
		}
	}
	pthread_mutex_unlock(&main_lock);
	return res;
}

/*
 * @brief semantic of pwrite but through the structure buffer
 *
 * @param circ structure for the file being written to
 * @param buffer where data to be written is stored
 * @param size of the data to write
 * @param offset from where the data must be written
 * @return number of bytes written (should be size unless error)
 */
static ssize_t bwrite(struct circ *cc, const void *buf
				     , size_t count, off_t offset)
{
	size_t sz;

	if (offset < cc->buf_off) {
		size_t to_write;
		/* write partially or totally "before" the buffer */
		/* Special case for "big_writes", skip buffering  */
		if (0 == cc->buf_used && count > BUF_SZ) {
			cc->buf_off = offset + count;
			return pwrite(cc->store_fh, buf, count, offset);
		}
		to_write = (cc->buf_off - offset > count)? count : 
							   cc->buf_off - offset;
		sz = pwrite(cc->store_fh, buf, to_write, offset);
		if (-1 == sz)
			return -1;
		if (sz == count)
			return count;
		buf = (char *)buf + sz;
		offset += sz;
	}
	else {
		sz = 0;
	}
	/* Note that count is never "after" the end of buffer because when
	 * there is a request after the "size" zero are written and will
	 * gradually move the buffer out. But count + offset can be, and
	 * it is then time to "flush" */
	if (offset + count - sz > cc->buf_off + BUF_SZ && 0 != cc->buf_used) {
		size_t to_write = offset - cc->buf_off;
		to_write = pwrite(cc->store_fh, cc->buf, to_write, cc->buf_off);
		if (-1 == to_write)
			return -1;
		cc->buf_off = offset;
		cc->buf_used = 0;
	}
	/* Big writes skip buffer */
	if (0 == cc->buf_used && count - sz >= BUF_SZ) {
		cc->buf_off = offset + count - sz;
		return pwrite(cc->store_fh, buf, count - sz, offset);
	}
	memcpy(cc->buf + offset - cc->buf_off, buf, count - sz);
	cc->buf_used += count - sz;
	return count;
}

/*
 * @brief fuse callback for write
 *
 * @param path to the file being written (unused)
 * @param buffer where data to be written is stored
 * @param size of the data to write
 * @param offset from where the data must be written
 * @param fuse file information pointer
 * @return number of bytes written (should be size unless error)
 */
static int circfs_write(const char *path_unused, const char *buf, size_t size,
			off_t offset, struct fuse_file_info *fi)
{
	(void)path_unused;
	struct circ *cc;
	ssize_t written, wrt2;
	size_t remains, forgotten = 0;
	off_t off;
	int err;

	cc = (struct circ *)((uintptr_t)(fi->fh));
	if (0 != pthread_mutex_lock(&main_lock))
		return -errno;

	if (cc->size > max_size && offset < cc->size - max_size) {
		/* Offset is before the "forget zone" */
		if (offset + size <= cc->size - max_size) {
			pthread_mutex_unlock(&main_lock);
			return size;	/* whole request is forgotten! */
		}
		/* Request is partially 'forgotten' */
		forgotten = cc->size - max_size - offset;
		buf    += forgotten;
		offset += forgotten;
		size   -= forgotten;
	}
	/* So, here offset is always inside the last circular buffer but if
	 * max_size was really small, there is more to forget! */
	if (size > max_size) {
 		offset    += size - max_size;
 		buf       += size - max_size;
 		forgotten += size - max_size;
 		size = max_size;
 	}
 	/* Now on the contrary, if this is beyond 'size' the non-written space
 	 * is filled with zeroes */
 	if (offset > cc->size) {
		off_t gap;
		gap = offset - cc->size;
		if (gap + size > max_size) {
			cc->size += gap + size - max_size;
			gap = offset - cc->size;
		}
		if (gap > 0) {
			#define ZERO_SZ 4096
			char zeroes[ZERO_SZ];
			memset(zeroes, '\0', (gap >= ZERO_SZ) ? ZERO_SZ : gap);
			off = cc->size % max_size;
			while (gap > 0) {
				size_t sz;
				sz = (gap > ZERO_SZ) ? ZERO_SZ : gap;
				if (off + sz > max_size)
					sz = max_size - off;
				if (bwrite(cc, zeroes, sz, off) < 0 ) {
					err = errno;
					pthread_mutex_unlock(&main_lock);
					return -errno;
				}
				off += sz;
				if (off == max_size)
					off = 0;
				gap -= sz;
			}
		}
		cc->size = offset;
	}
	/* Now proceed with writing what remains */
	off     = offset % max_size;
	remains = max_size - off;
	if (remains >= size) {
		/* The write does not overlap the end */
		written = bwrite(cc, buf, size, off);
		err = errno;
	}
	else {
		/* Overlapping, needs 2 writes */
		written = pwrite(cc->store_fh, buf, remains, off);
		err = errno;
		if (-1 != written) {
			wrt2 = bwrite(cc, buf + remains, size - remains, 0);
			err = errno;
			if (-1 == wrt2) {
				if (offset + remains > cc->size)
					cc->size = offset + remains;
				written = -1;
			}
			else {
				written += wrt2;
			}
		}
	}
	pthread_mutex_unlock(&main_lock);
	if (-1 == written) {
		return -err;
	}
	else {
		if (offset + written > cc->size)
			cc->size = offset + written;
		return written + forgotten;
	}
}

/*
 * @brief rewrites a circular file
 *
 * When a file is completely released, rewriting happens if needed.
 *
 * @param circular file pointer
 * @return none
 */
static void rewrite(struct circ *cc)
{
	int fh;
	off_t  off, to_copy;
	size_t sz, path_sz;
	char buf[ZERO_SZ];

	if (0 != cc->buf_used)
		sz = pwrite(cc->store_fh, cc->buf, cc->buf_used, cc->buf_off);

	if (cc->size <= max_size)
		return;	/* Did not wrap yet, nothing to rewrite */

	off = cc->size % max_size;
	if (0 == off)
		return;/* Being lucky, nothing to rewrite: no wrapping around */


	path_sz = strlen(cc->path) + 1;
	char old_path[storage_dir_strlen + path_sz];
	char new_path[storage_dir_strlen + path_sz + 1];

	memcpy(new_path, storage_dir, storage_dir_strlen);
	new_path[storage_dir_strlen    ] = '/';
	new_path[storage_dir_strlen + 1] = '.';
	strcpy(new_path + storage_dir_strlen + 2, cc->path + 1);
	fh = open(new_path, O_WRONLY | O_CREAT | O_EXCL, cc->mode);
	if (-1 == fh)
		return; /* Just don't rewrite if open fails! */

	to_copy = max_size;
	lseek(cc->store_fh, off, SEEK_SET);
	while(to_copy > 0) {
		sz = (to_copy > ZERO_SZ) ? ZERO_SZ : to_copy;
		if (off + sz > max_size)
			sz = max_size - off;
		if (-1 == read(cc->store_fh, buf, sz))
			break;
		if (-1 == write(fh, buf, sz))
			break;
		to_copy -= sz;
		off += sz;
		if (off == max_size) {
			off = 0;
			lseek(cc->store_fh, 0, SEEK_SET);
		}
	}
	close(cc->store_fh);
	cc->store_fh = 0;
	close(fh);
	if (to_copy > 0)
		return; 	/* When read of write failed */

	memcpy(old_path, storage_dir, storage_dir_strlen);
	strcpy(old_path + storage_dir_strlen, cc->path);
	if (0 != rename(new_path, old_path))
		unlink(new_path); /* Don't test result since there is nothing
				   more to do if rename failed and unlink too */
}

/*
 * @brief fuse callback for release
 *
 * @param path of the file to be released (not used)
 * @param address of the fuse file information
 * @return 0 if Ok or error code (negative)
 */
static int circfs_release(const char *path_unused, struct fuse_file_info *fi)
{
	(void)path_unused;
	struct circ *cc, **pcc;

	cc = (struct circ *)((uintptr_t)(fi->fh));
	if (0 != pthread_mutex_lock(&main_lock))
		return -EIO;
	cc->n_open--;
	if (0 == cc->n_open) {
		/* Remove cc from the list */
		for (pcc = &head; *pcc != cc; pcc = &((*pcc)->next));
		*pcc = cc->next;
	}

	/* unlock as soon as possible */
	pthread_mutex_unlock(&main_lock);

	if (0 == cc->n_open) {
		rewrite(cc);
		if (0 != cc->store_fh)
			close(cc->store_fh);
		free(cc);
	}
	return 0;
}

static struct fuse_operations circfs_oper = {
	.getattr	= circfs_getattr,
	.readdir	= circfs_readdir,
	.create		= circfs_create,
	.release	= circfs_release,
	.write		= circfs_write,
};

/*
 * @brief checks directory path
 *
 * Note: strerror is not thread safe, but more portable than strerror_r, and
 *       since this is used before daemonizing and multithreading, there is
 *       no race condition here.
 *
 * @param path to check
 * @return none
 */
static int check_mount_path(const char *path)
{
	int err;

	if (-1 == stat(path, &mount_st)) {
		err = errno;
		fprintf(stderr , "getting stats of `%s`: %s\n"
				, path, strerror(err));
		return err;
	}
	if ( ! S_ISDIR(mount_st.st_mode) ) {
		fprintf(stderr, "`%s` is not a directory\n",  path);
		return ENOTDIR;
	}
	mount_st.st_nlink = 2;
	return 0;
}


/*
 * @brief main
 *
 * Handles arguments and starts fuse
 *
 * @param argument count
 * @param arguments string array
 * @return error code
 */
int main(int argc, char *argv[])
{
	int i, err;
	char size_unit, extra;

	if (argc < 4) {
		fprintf(stderr,
			"Usage: %s storage_dir mountpoint max_size [fuse options]\n",
			argv[0]);
		return 1;
	}
	if (0 == strcmp(argv[1], argv[2])) {
		fprintf(stderr,
			"storage directory and mountpoint cannot be the same: %s\n",
			argv[1]);
		return 1;
	}
	printf("storage_dir: %s\n", argv[1]);
	err = check_mount_path(argv[1]);
	if (0 != err)
		return err;
	printf("mountpoint : %s\n", argv[2]);
	err = check_mount_path(argv[2]);
	if (0 != err)
		return err;

	extra='\0';
	size_unit='b';
	if ( 0 == sscanf(argv[3], "%ju%c%c", (uintmax_t *)&max_size
					   , &size_unit, &extra) ) {
		fprintf(stderr,
			"Invalid number: no integer for 'max_size' `%s`\n",
			argv[3]);
		return 1;
	}
	if ( 0 == max_size ) {
		fprintf(stderr, "Zero is not suitable as max_size!\n");
		return 1;
	}
	if ( '\0' != extra || NULL == strchr(units,size_unit) ) {
		fprintf(stderr,
			"Invalid number: invalid units or extraneous characters in: `%s`\n",
			argv[3]);
		return 1;
	}
	max_size *= multiple[ (strchr(units,size_unit) - units) ];
	printf("max_size   : %llu\n", (unsigned long long)max_size);

	/*
	 Starting the fuse mount!
	 */
	storage_dir = argv[1];
	storage_dir_strlen = strlen(storage_dir);
	argv[1] = argv[2];
	for (i = 4; i < argc; i++)
		argv[i - 2] = argv[i];
	fuse_main(argc - 2, argv, &circfs_oper, NULL);

	return 0;
}
