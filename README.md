# circFS

This single file implements a basic "circular buffer file"

## Feature:
	Apparently, GNU/Linux does not have an all purpose circular file tool.

	https://serverfault.com/questions/133320/turning-a-log-file-into-a-sort-of-circular-buffer
	https://unix.stackexchange.com/questions/392908/circular-log-in-linux
	
	The goal is to limit the size of evergrowing log to a maximum amount,
	loosing in the process oldest entries if the amount is exceeded.

	There is dmesg for kernel log, and some tools like emblog which is
	very limited in size (targetted at "embedded" systems) and tools in
	daemontools that rotate logs but also limited to 16Mb per log file.
	
	So I decided to do that basic tool with fuse.
	It works a bit like encfs, the user provides:
	- a real directory were raw logs will be written to
	- a mount directory that is where "circular" logs go
	- a maximum size
	
	When writing a file to the mountpoint, the same file will be written
	to the real directory. The "real" file will be kept at the maximum size
	and start to "rotate" from there.
	The file on the mount would on the contrary seem to grow indefinitely.
	When the file is closed, the real file is re-written so that it can
	be displayed with standard tools (cat and all).
	So in fact the space needed on the storage is twice the maximum for
	each file written at the time of rewrite.
	
	The user can write as many files as needed on the mountpoint, they
	all have the same maximum size.
	
	Since this is (for now) very basic, the fuse driver does NOT provide
	reading of file. The files (although "circular") can be accessed
	in the real directory instead.
	 
	So in fact, at the moment this is a "Write-only" fuse driver. ;-)
	
	Note that this is fuse, so it does not have any notion of "line" and
	simply write buffers it receives.
	- When a buffer is received passed the current end of file, the gap if
	  filled with zeroes
	- When a buffer is received before the beginning of the rotated file,
	  the buffer is silently discarded.
	- Overlap/rewrite is possible inside the rotated zone, as long as the
	  storage directory allows random write.
	- Since there is no notion of line, it is possible that the first line
	  (when writing "lines") of the rewrite is only a partial line.
	- The rewritten file will have exactly the maximum size when rotation
	  has happened (otherwise rewriting is not necessary!)

## Usage:
	circfs storage_dir mountpoint max_size_per_file [fuse options]
	
	The size expression can use standard multipliers: k K, m M, g G, t T
	k = 1000
	K = 1024
	etc...


## Typical use case:
	Limit log files.
	On RaspberryPi with 2GB of /tmp RAM it is too small to track errors
	on other fuse driver (see 1fichierfs with debug).
	Limiting to 500M is much enough to see the last events before a
	potential crash

## Compile with:
	You only need one file to compile: circfs.c
	
	cc -O2 -Wall -std=c99 -Wpedantic circfs.c `pkg-config fuse --cflags --libs` -o circfs

## Compile dependancy:
	You must have libfuse-dev installed, and standard compile tools (generally
	already installed as default in your distribution)

	Typically install libfuse-dev with: 

	(Debian/Ubuntu) sudo apt-get install libfuse-dev

	(Fedora)        sudo dnf install fuse-devel
	... or adapt with the package manager that comes with your distribution.


## License GPL V3
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
