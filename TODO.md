# circFS /TODO
Fuse filesystem that implements a "circular buffer file"

## Priority:
- Implement read

## Enhancements:
- better (finer) locking
- isolate rewrite into a separate thread to avoid blocking for long times


## Nice to have
- better parameter management and possible make rewrite an option

## Continue to clean the code!
